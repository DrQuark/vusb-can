/* Name: main.c
   Project: hid-data, example how to use HID for data transfer
   Author: Christian Starkjohann
   Creation Date: 2008-04-11
   Tabsize: 4
   Copyright: (c) 2008 by OBJECTIVE DEVELOPMENT Software GmbH
   License: GNU GPL v2 (see License.txt), GNU GPL v3 or proprietary (CommercialLicense.txt)
   This Revision: $Id: main.c 692 2008-11-07 15:07:40Z cs $
*/

/*
  This example should run on most AVRs with only little changes. No special
  hardware resources except INT0 are used. You may have to change usbconfig.h for
  different I/O pins for USB. Please note that USB D+ must be the INT0 pin, or
  at least be connected to INT0 as well.
*/

// VENDOR TYPE REQUEST
// -------------------
// bRequest == CUSTOM_RQ_GET_STATUS
// (wIndex)0 -> GET CAN MODE (1 ISOTP, 0 MONITOR)
// (wIndex)1 -> GET CAN ISO_TP TX ADDR
// (wIndex)2 -> GET CAN ISO_TP RX ADDR
// (wIndex)3 -> CAN ISO_TP DATA AVAILABLE FOR READING (LENGTH) (wValue)
//  -> 0xFFFF means CAN read error
//  -> 0xFFFE means message not arrived
//  >= 0 data ready

// bRequest == CUSTOM_RQ_SET_STATUS
// (wIndex)0 -> SET CAN MODE (1 ISOTP, 0 MONITOR)
// (wIndex)1 -> SET CAN ISO_TP TX ADDR (wValue)
// (wIndex)2 -> SET CAN ISO_TP RX ADDR (wValue)
// (wIndex)3 -> CAN ISO_TP DATA DATA READY FOR WRITING (LENGTH) (wValue)
// (wIndex)4 -> CAN ISO_TP FILTER ADDRESS SET (0x0000 clear filter)

// CLASS TYPE REQUEST
// bRequest == USBRQ_HID_GET_REPORT
// Device Buffer -> Host
// bRequest == USBRQ_HID_SET_REPORT
// Host -> Device Buffer

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>  /* for sei() */
#include <util/delay.h>     /* for _delay_ms() */
#include <avr/eeprom.h>

#include <avr/pgmspace.h>   /* required by usbdrv.h */
#include "usbdevice.h"
#include "ecu.h"

uint8_t g_can_buffer[1024];
uint8_t*g_can_buffer2 = g_can_buffer + 512;
bool    g_iso_tp_mode = false;
bool    g_iso_tp_send_ready = false;
bool    g_iso_tp_receive_ready = false;
short   g_iso_tp_data_len_tx = 0;
short   g_iso_tp_data_len_rx = 0;
short   g_offset = 0;
#define CUSTOM_RQ_SET_STATUS    1
#define CUSTOM_RQ_GET_STATUS    2

extern ECU CAN_ECU;

/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */
#define HID_ReportCountS(x)     0x96,(x&0xFF),((x>>8)&0xFF)

const PROGMEM char usbHidReportDescriptor[35] = {    /* USB report descriptor */
  0x06, 0x00, 0xff,              // USAGE_PAGE (Generic Desktop)
  0x09, 0x01,                    // USAGE (Vendor Usage 1)
  0xa1, 0x01,                    // COLLECTION (Application)

  0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
  0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (512)
  0x75, 0x08,                    //   REPORT_SIZE (8)

  0x85, 0x00,					           // 	REPORT ID (19)
  HID_ReportCountS(0x1ff),
  0x09, 0x00,                    //   USAGE (Undefined)
  0xb2, 0x02, 0x01,              //   FEATURE (Data,Var,Abs,Buf)

  0x85, 0x14,					           // 	REPORT ID (20)
  HID_ReportCountS(0x1ff),       //   REPORT_COUNT (512)
  0x09, 0x00,                    //   USAGE (Undefined)
  0xb2, 0x02, 0x01,              //   FEATURE (Data,Var,Abs,Buf)

  0xc0,                          // END_COLLECTION                     // END_COLLECTION
};

#ifdef __cplusplus
extern "C" {
#endif

static unsigned    currentAddress;
static unsigned    bytesRemaining;

/* usbFunctionRead() is called when the host requests a chunk of data from
   the device. For more information see the documentation in usbdrv/usbdrv.h.
   Device -> Host
*/
uchar   usbFunctionRead(uchar *data, uchar len)
{
  if (len > bytesRemaining)
    len = bytesRemaining;
  memcpy((void*)data, (void*)g_can_buffer + g_offset + currentAddress, len);
  currentAddress += len;
  bytesRemaining -= len;
  
  return len;
}

/* usbFunctionWrite() is called when the host sends a chunk of data to the
   device. For more information see the documentation in usbdrv/usbdrv.h.
   Host -> Device
*/
uchar   usbFunctionWrite(uchar *data, uchar len)
{
  if (bytesRemaining == 0){
    return 1;               /* end of transfer */
  }
  if (len > bytesRemaining)
    len = bytesRemaining;
  memcpy((void*)g_can_buffer + g_offset + currentAddress, (void*)data, len);
  currentAddress += len;
  bytesRemaining -= len;
  
  return bytesRemaining == 0; /* return 1 if this was the last chunk */
}


usbMsgLen_t usbFunctionSetup(uchar data[8])
{
  usbRequest_t    *rq = (usbRequest_t*)((void *)data);
  static short dataBuffer[1];
  
  if ( (rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS ) {
    /* wValue: ReportType (highbyte), ReportID (lowbyte) */
    if (rq->wIndex.word == 1) // ReportId
      g_offset = 0;
    else
      g_offset = 512;
    if (rq->bRequest == USBRQ_HID_GET_REPORT) {
      bytesRemaining = 0x1ff;
      if (bytesRemaining > rq->wLength.word)
        // The host requests less than default
        bytesRemaining = rq->wLength.word;
      currentAddress = 0;
      return USB_NO_MSG;
    } else if (rq->bRequest == USBRQ_HID_SET_REPORT) {
      bytesRemaining = 0x1ff;
      if (bytesRemaining > rq->wLength.word)
        // The host requests less than default
        bytesRemaining = rq->wLength.word;
      currentAddress = 0;
      return USB_NO_MSG;
    }
  } else if ( (rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_VENDOR ) {
    // Handle CAN buffer here
    // Note that the RX/TX buffer is the same to not waste memory
    if (rq->bRequest == CUSTOM_RQ_SET_STATUS) {
      // Handle isotp requests
      if (rq->wIndex.word == 0) {
        g_iso_tp_mode = rq->wValue.bytes[0] ? true : false;
      }
      if (rq->wIndex.word == 1) {
        CAN_ECU.set_txaddress(rq->wValue.word);
      }
      if (rq->wIndex.word == 2) {
        CAN_ECU.set_rxaddress(rq->wValue.word);
      }
      if (rq->wIndex.word == 3){
        g_iso_tp_data_len_tx = rq->wValue.word;
        g_iso_tp_data_len_rx = 0;
        g_iso_tp_send_ready = true;
      }
      if (rq->wIndex.word == 4) {
        CAN_ECU.set_rxfilter(rq->wValue.word);
      }
    } else if (rq->bRequest == CUSTOM_RQ_GET_STATUS) {
      // Handle CAN parameters here
      if (rq->wIndex.word == 0) {
        dataBuffer[0] = g_iso_tp_mode;
        return 1;
      }
      if (rq->wIndex.word == 1) {
        dataBuffer[0] = CAN_ECU.get_txaddress();
        usbMsgPtr = (uint8_t*)dataBuffer;
        return 2;
      }
      if (rq->wIndex.word == 2) {
        dataBuffer[0] = CAN_ECU.get_rxaddress();
        usbMsgPtr = (uint8_t*)dataBuffer;
        return 2;
      }
      if (rq->wIndex.word == 3) {
        dataBuffer[0] = g_iso_tp_receive_ready ? g_iso_tp_data_len_rx : 0xFFFE;
        usbMsgPtr = (uint8_t*)dataBuffer;
        return 2;
      }
    }
  }
  return 0;
}
#ifdef __cplusplus
} // extern "C"
#endif

