/*
   Based on Obdev's AVRUSB code and under the same license.

   TODO: Make a proper file header. :-)
*/
#ifndef __UsbDevice_h__
#define __UsbDeviceh__

#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <string.h>
#include "usbdevice.h"

typedef uint8_t byte;

#include <util/delay.h>

class UsbGenericDevice {
  public:
    UsbGenericDevice () {
    }

    void begin() {
      // disable timer 0 overflow interrupt (used for millis)
      // Trying without disabling first (millis code in hardwarde/arduino/avr/cores/arduino/wiring.c)
      // Maybe trying to make it NOBLOCK
      //TIMSK0&=~(1<<TOIE0);

      cli();

      usbInit();

      usbDeviceDisconnect();
      uchar   i;
      i = 0;
      while (--i) {           /* fake USB disconnect for > 250 ms */
        _delay_ms(1);
      }
      usbDeviceConnect();

      sei();
    }

    // TODO: Deprecate update
    void update() {
      refresh();
    }

    void refresh() {
      usbPoll();
    }

};

UsbGenericDevice UsbDevice;

#endif // __UsbDevice_h__
