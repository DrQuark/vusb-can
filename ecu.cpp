#include "ecu.h"

ECU::ECU(MCP_CAN* mcp_can, uint8_t mcp_int_pin, uint8_t* buffer, short rxAddr, short txAddr) : m_iso_tp(mcp_can, mcp_int_pin)
{
  m_canmsg.tx_id   = txAddr;
  m_canmsg.rx_id   = rxAddr;
  m_canmsg.Buffer  = buffer;
}

ECU::~ECU()
{
}

void ECU::set_rxaddress(short rxAddr)
{
  m_canmsg.rx_id = rxAddr;
}

void ECU::set_txaddress(short txAddr)
{
  m_canmsg.tx_id = txAddr;
}

bool
ECU::send_message(short len)
{
  if (len > MAX_MSGBUF)
    return false;
    
  m_canmsg.len = len;
  return (m_iso_tp.send(&m_canmsg) == 0);
}

bool
ECU::receive()
{
  m_canmsg.len = 0;
  
  if (m_iso_tp.receive(&m_canmsg) != 0)
    return false;

  m_rxmessage_length = m_canmsg.len;
  
  if (m_canmsg.len == 0)
    return false;
    
  return true;
}

short 
ECU::receive_message()
{
  if ( !receive() )
    return 0xFFFF;

  return m_rxmessage_length;
}

void
ECU::set_rxfilter(short rxFilter)
{
  m_iso_tp.set_rxfilter(rxFilter);
}

