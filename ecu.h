#ifndef ECU_H
#define ECU_H

#include "iso_tp.h"
#include "mcp_can.h"

class ECU
{
    IsoTp   m_iso_tp;
    struct  Message_t m_canmsg;
    short   m_rxmessage_length;

  protected:
    bool receive();
        
  public:
    ECU(MCP_CAN* mcp_can, uint8_t mcp_int_pin, uint8_t* buffer, short rxAddr, short txAddr);
    ~ECU();

    void set_rxaddress(short txAddr);
    void set_txaddress(short rxAddr);

    short get_rxaddress(){return m_canmsg.rx_id;}
    short get_txaddress(){return m_canmsg.tx_id;}

    void  set_rxfilter(short rxFilter);
    
    short receive_message();
    bool send_message(short len);
};

#endif
