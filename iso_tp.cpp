#include "Arduino.h"
#include "iso_tp.h"
#include "mcp_can.h"
#include "mcp_can_dfs.h"

IsoTp::IsoTp(MCP_CAN* bus, uint8_t mcp_int)
{
  _mcp_int = mcp_int;
  _bus = bus;
}

uint8_t IsoTp::can_send(uint32_t id, uint8_t len, uint8_t *data)
{
  return _bus->sendMsgBuf(id, 0, len, data);
}

uint8_t IsoTp::can_receive(void)
{
  bool msgReceived;

  if (_mcp_int)
    msgReceived = (!digitalRead(_mcp_int));                     // IRQ: if pin is low, read receive buffer
  else
    msgReceived = (_bus->checkReceive() == CAN_MSGAVAIL);       // No IRQ: poll receive buffer

  if (msgReceived)
  {
    memset(rxBuffer, 0, sizeof(rxBuffer));     // Cleanup Buffer
    _bus->readMsgBuf(&rxId, &rxLen, rxBuffer); // Read data: buf = data byte(s)
    return true;
  }
  else return false;
}

uint8_t IsoTp::send_fc(struct Message_t *msg)
{
  uint8_t TxBuf[8];// = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  memset(TxBuf, 0, 8);
  // FC message high nibble = 0x3 , low nibble = FC Status
  TxBuf[0] = (N_PCI_FC | msg->fc_status);
  TxBuf[1] = msg->blocksize;
  /* fix wrong separation time values according spec */
  if ((msg->min_sep_time > 0x7F) && ((msg->min_sep_time < 0xF1)
                                     || (msg->min_sep_time > 0xF9))) msg->min_sep_time = 0x7F;
  TxBuf[2] = msg->min_sep_time;
  return can_send(msg->tx_id, 8, TxBuf);
}

uint8_t IsoTp::send_sf(struct Message_t *msg) //Send SF Message
{
  uint8_t TxBuf[8];// = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  memset(TxBuf, 0, 8);
  // SF message high nibble = 0x0 , low nibble = Length
  TxBuf[0] = (N_PCI_SF | msg->len);
  memcpy(TxBuf + 1, msg->Buffer, msg->len);
  //  return can_send(msg->tx_id,msg->len+1,TxBuf);// Add PCI length
  return can_send(msg->tx_id, 8, TxBuf); // Always send full frame
}

uint8_t IsoTp::send_ff(struct Message_t *msg) // Send FF
{
  uint8_t TxBuf[8];// = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  memset(TxBuf, 0, 8);
  msg->seq_id = 1;

  TxBuf[0] = (N_PCI_FF | ((msg->len & 0x0F00) >> 8));
  TxBuf[1] = (msg->len & 0x00FF);
  memcpy(TxBuf + 2, msg->Buffer, 6);         // Skip 2 Bytes PCI
  return can_send(msg->tx_id, 8, TxBuf);     // First Frame has full length
}

uint8_t IsoTp::send_cf(struct Message_t *msg) // Send SF Message
{
  uint8_t TxBuf[8];// = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  memset(TxBuf, 0, 8);
  uint16_t len = 7;

  TxBuf[0] = (N_PCI_CF | (msg->seq_id & 0x0F));
  if (msg->len > 7) len = 7; else len = msg->len;
  memcpy(TxBuf + 1, msg->Buffer, len);     // Skip 1 Byte PCI
  //return can_send(msg->tx_id,len+1,TxBuf); // Last frame is probably shorter
  // than 8 -> Signals last CF Frame
  return can_send(msg->tx_id, 8, TxBuf);   // Last frame is probably shorter
  // than 8, pad with 00
}

void IsoTp::fc_delay(uint8_t sep_time)
{
  if (sep_time < 0x80)
    delay(sep_time);
  else
    delayMicroseconds((sep_time - 0xF0) * 100);
}

uint8_t IsoTp::rcv_sf(struct Message_t* msg)
{
  /* get the SF_DL from the N_PCI byte */
  msg->len = rxBuffer[0] & 0x0F;
  /* copy the received data bytes */
  memcpy(msg->Buffer, rxBuffer + 1, msg->len); // Skip PCI, SF uses len bytes
  msg->tp_state = ISOTP_FINISHED;

  return 0;
}

uint8_t IsoTp::rcv_ff(struct Message_t* msg)
{
  msg->seq_id = 1;

  /* get the FF_DL */
  msg->len = (rxBuffer[0] & 0x0F) << 8;
  msg->len += rxBuffer[1];
  rest = msg->len;

  /* copy the first received data bytes */
  memcpy(msg->Buffer, rxBuffer + 2, 6); // Skip 2 bytes PCI, FF must have 6 bytes!
  rest -= 6; // Restlength

  msg->tp_state = ISOTP_WAIT_DATA;

  /* send our first FC frame with Target Address*/
  struct Message_t fc;
  fc.tx_id = msg->tx_id;
  fc.fc_status = ISOTP_FC_CTS;
  fc.blocksize = 0;
  fc.min_sep_time = 0;
  return send_fc(&fc);
}

uint8_t IsoTp::rcv_cf(struct Message_t* msg)
{
  //Handle Timeout
  //If no Frame within 250ms change State to ISOTP_IDLE
  uint32_t delta = millis() - wait_cf;

  if ((delta >= TIMEOUT_FC) && msg->seq_id > 1)
  {
    msg->tp_state = ISOTP_IDLE;
    return 1;
  }
  wait_cf = millis();

  if (msg->tp_state != ISOTP_WAIT_DATA) return 0;

  if ((rxBuffer[0] & 0x0F) != (msg->seq_id & 0x0F))
  {
    msg->tp_state = ISOTP_IDLE;
    msg->seq_id = 1;
    return 1;
  }

  if (rest <= 7) // Last Frame
  {
    memcpy(msg->Buffer + 6 + 7 * (msg->seq_id - 1), rxBuffer + 1, rest); // 6 Bytes in FF +7
    msg->tp_state = ISOTP_FINISHED;                         // per CF skip PCI
  }
  else
  {
    memcpy(msg->Buffer + 6 + 7 * (msg->seq_id - 1), rxBuffer + 1, 7); // 6 Bytes in FF +7
    // per CF
    rest -= 7; // Got another 7 Bytes of Data;
  }

  msg->seq_id++;

  return 0;
}

uint8_t IsoTp::rcv_fc(struct Message_t* msg)
{
  uint8_t retval = 0;

  if (msg->tp_state != ISOTP_WAIT_FC && msg->tp_state != ISOTP_WAIT_FIRST_FC)
    return 0;

  /* get communication parameters only from the first FC frame */
  if (msg->tp_state == ISOTP_WAIT_FIRST_FC)
  {
    msg->blocksize = rxBuffer[1];
    msg->min_sep_time = rxBuffer[2];

    /* fix wrong separation time values according spec */
    if ((msg->min_sep_time > 0x7F) && ((msg->min_sep_time < 0xF1)
                                       || (msg->min_sep_time > 0xF9))) msg->min_sep_time = 0x7F;
  }

  switch (rxBuffer[0] & 0x0F)
  {
    case ISOTP_FC_CTS:
      msg->tp_state = ISOTP_SEND_CF;
      break;
    case ISOTP_FC_WT:
      fc_wait_frames++;
      if (fc_wait_frames >= MAX_FCWAIT_FRAME)
      {
        fc_wait_frames = 0;
        msg->tp_state = ISOTP_IDLE;
        retval = 1;
      }
      break;
    case ISOTP_FC_OVFLW:
    default:
      msg->tp_state = ISOTP_IDLE;
      retval = 1;
  }
  return retval;
}

uint8_t IsoTp::send(Message_t* msg)
{
  uint8_t bs = false;
  uint32_t delta = 0;
  uint8_t retval = 0;

  msg->tp_state = ISOTP_SEND;

  while (msg->tp_state != ISOTP_IDLE && msg->tp_state != ISOTP_ERROR)
  {
    bs = false;

    switch (msg->tp_state)
    {
      case ISOTP_IDLE         :  break;
      case ISOTP_SEND         :
        if (msg->len <= 7)
        {
          retval = send_sf(msg);
          msg->tp_state = ISOTP_IDLE;
        }
        else
        {
          if (!(retval = send_ff(msg))) // FF complete
          {
            msg->Buffer += 6;
            msg->len -= 6;
            msg->tp_state = ISOTP_WAIT_FIRST_FC;
            fc_wait_frames = 0;
            wait_fc = millis();
          }
        }
        break;
      case ISOTP_WAIT_FIRST_FC:
        delta = millis() - wait_fc;
        if (delta >= TIMEOUT_FC)
        {
          msg->tp_state = ISOTP_IDLE;
          retval = 1;
        }
        break;
      case ISOTP_WAIT_FC      :
        break;
      case ISOTP_SEND_CF      :
        while (msg->len > 7 & !bs)
        {
          fc_delay(msg->min_sep_time);
          if (!(retval = send_cf(msg)))
          {
            if (msg->blocksize > 0)
            {
              if (!(msg->seq_id % msg->blocksize))
              {
                bs = true;
                msg->tp_state = ISOTP_WAIT_FC;
              }
            }
            msg->seq_id++;
            msg->seq_id %= 16;
            msg->Buffer += 7;
            msg->len -= 7;
          }
        }
        if (!bs)
        {
          fc_delay(msg->min_sep_time);
          retval = send_cf(msg);
          msg->tp_state = ISOTP_IDLE;
        }
        break;
      default                 :  break;
    }


    if (msg->tp_state == ISOTP_WAIT_FIRST_FC ||
        msg->tp_state == ISOTP_WAIT_FC)
    {
      if (can_receive())
      {
        if (rxId == msg->rx_id)
        {
          retval = rcv_fc(msg);
          memset(rxBuffer, 0, sizeof(rxBuffer));
        }
      }
    }
  }

  return retval;
}

void IsoTp::set_rxfilter(short rxFilter)
{
  if (rxFilter == 0x0000){
    _bus->init_Mask(0, 0x0);
    _bus->init_Filt(0, 0x0);
    return;
  }
  _bus->init_Mask(0, 0x7FF);
  _bus->init_Filt(0, rxFilter);
}

uint8_t IsoTp::receive(Message_t* msg)
{
  uint8_t n_pci_type = 0;
  uint32_t delta = 0;

  wait_session = millis();
  msg->tp_state = ISOTP_IDLE;

  while (msg->tp_state != ISOTP_FINISHED && msg->tp_state != ISOTP_ERROR)
  {
    delta = millis() - wait_session;
    if (delta >= TIMEOUT_SESSION)
    {
      return 1;
    }

    if (can_receive())
    {
      if (rxId == msg->rx_id)
      {
        n_pci_type = rxBuffer[0] & 0xF0;

        switch (n_pci_type)
        {
          case N_PCI_FC:
            /* tx path: fc frame */
            rcv_fc(msg);
            break;

          case N_PCI_SF:
            /* rx path: single frame */
            rcv_sf(msg);
            //          msg->tp_state=ISOTP_FINISHED;
            break;

          case N_PCI_FF:
            /* rx path: first frame */
            rcv_ff(msg);
            //          msg->tp_state=ISOTP_WAIT_DATA;
            break;
            break;

          case N_PCI_CF:
            /* rx path: consecutive frame */
            rcv_cf(msg);
            break;
        }
        memset(rxBuffer, 0, sizeof(rxBuffer));
      }
    }
  }

  return 0;
}
