#include "monitor.h"

Can_monitor::Can_monitor(MCP_CAN* c, uint8_t* canbuffer, uint8_t mcp_int) : m_can(c)
{
  m_mcp_int = mcp_int;
  m_canbuffer = canbuffer;
}

Can_monitor::~Can_monitor()
{

}

bool
Can_monitor::monitor()
{
  // Make a hard copy of the vehicle real time parameters
  // that can be used for later use
  uint8_t recv_frame[8];
  bool msgReceived;
  
  if (m_mcp_int){
    msgReceived = (!digitalRead(m_mcp_int));
  } else{
    msgReceived = (CAN_MSGAVAIL == m_can->checkReceive());
  }
  
  if (msgReceived) {
    uint32_t can_id;
    uint8_t len;
    if (m_can->readMsgBuf(&can_id, &len, recv_frame) == CAN_OK) {
    switch (can_id) {
        case UCH_GENERAL:
          noInterrupts();
          memcpy((void*)m_canbuffer, (void*)recv_frame, 8);
          interrupts();
          return true;
        case UCH_SYSTEM:
          memcpy((void*)m_canbuffer + 8, (void*)recv_frame, 8);
          return true;
        case CLUSTER_BACKUP:
          memcpy((void*)m_canbuffer + 16, (void*)recv_frame, 8);
          return true;
        case ECM_GENERAL:
          memcpy((void*)m_canbuffer + 24, (void*)recv_frame, 8);
          return true;
        case BRAKE_GENERAL:
          memcpy((void*)m_canbuffer + 32, (void*)recv_frame, 8);
          return true;
        case ECM_TORQUE:
          memcpy((void*)m_canbuffer + 40, (void*)recv_frame, 8);
          return true;
        default:
          break;
      }
    }
  }
  return false;
}

