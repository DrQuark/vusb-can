#ifndef MONITOR_H
#define MONITOR_H

#include "mcp_can.h"

#define UCH_SYSTEM      0x035d
#define UCH_GENERAL     0x060d // 8
#define CLUSTER_BACKUP  0x0715 // 16
#define ECM_GENERAL     0x0551 // 24
#define BRAKE_GENERAL   0x0354 // 32
#define ECM_TORQUE      0x0181 // 40

class Can_monitor
{
    MCP_CAN*  m_can;
    uint8_t   m_mcp_int;
    uint8_t*  m_canbuffer;
  public:
    // if interrupt (INT) not used, mcp_int = 0
    Can_monitor(MCP_CAN* it, uint8_t* canbuffer, uint8_t mcp_int = 0);
    ~Can_monitor();

    bool monitor();
};

#endif
