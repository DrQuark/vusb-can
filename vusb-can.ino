#include "UsbDevice.h"
#include "mcp_can.h"
#include "ecu.h"
#include "monitor.h"
#include "MsTimer2.h"

// Board pins assignations
#define MCP_INT       9  // ARDUINO PIN PB1
#define MCP_CS        10 // ARDUINO PIN PB2
#define CAN_LED_PIN   8  // ARDUINO PIN PB0

// CAN CONFIG
#define CAN_BAUDRATE  CAN_500KBPS
#define MCP_OSC_SPEED MCP_8MHZ

// Global extern variables
extern uint8_t  g_can_buffer[1024];
extern uint8_t* g_can_buffer2;
extern bool     g_iso_tp_mode;
extern short    g_iso_tp_data_len_tx;
extern short    g_iso_tp_data_len_rx;
extern bool     g_iso_tp_send_ready;
extern bool     g_iso_tp_receive_ready;

// Globals
bool            g_can_init;
unsigned long   time;
unsigned long   blink_rate = 500;
bool            g_ledstate = false;


// Global classes
MCP_CAN CAN0(MCP_CS);
ECU CAN_ECU(&CAN0, MCP_INT, g_can_buffer, 0x0, 0x0);
Can_monitor MONITOR(&CAN0, g_can_buffer, MCP_INT);

// USB poll interrupt routine (each 5ms)
void usbPollIntr(void)
{
  UsbDevice.refresh();
}

void setup() {
  time = millis();
  // Turn off led
  pinMode(CAN_LED_PIN, OUTPUT);
  pinMode(MCP_INT, INPUT);
  digitalWrite(CAN_LED_PIN, LOW);
  g_can_init = false;

  // Prepare CAN module
  if (CAN0.begin(MCP_ANY, CAN_BAUDRATE, MCP_OSC_SPEED) == CAN_OK){
      CAN0.setMode(MCP_NORMAL);
      g_can_init = true;
  } else {
    // Fast led blinking if bad init, useful for debug
    g_can_init = false;
    blink_rate = 200;
  }

  // Clear buffer
  for (int i = 0; i < 0x1ff; ++i)
    g_can_buffer[i] = 0x00;

  // Usb polling timer interrupt
  MsTimer2::set(5, usbPollIntr);
  UsbDevice.begin();
  MsTimer2::start();
}

void loop() {
  if (!g_iso_tp_mode && MONITOR.monitor()){
    // Blink led to inform activity
    digitalWrite(CAN_LED_PIN, g_ledstate ? HIGH : LOW);
    // Blink on CAN ACK
    g_ledstate = !g_ledstate;
  } else if (g_iso_tp_mode){
    if (g_iso_tp_send_ready){
      // We have data to send to CAN
      CAN_ECU.send_message(g_iso_tp_data_len_tx);
      
      // Reset tx flags
      g_iso_tp_data_len_tx = 0;
      g_iso_tp_send_ready = false;
      g_iso_tp_receive_ready = false;
      
      // Receive message
      g_iso_tp_data_len_rx = CAN_ECU.receive_message();
      // Set data length
      if (g_iso_tp_data_len_rx){
        // Set receive flag ready
        g_iso_tp_receive_ready = true;
      }
    }
  }
  if (millis() - time > blink_rate){
    time = millis();
    digitalWrite(CAN_LED_PIN, g_ledstate ? HIGH : LOW);
    g_ledstate = !g_ledstate;    
  }
}


